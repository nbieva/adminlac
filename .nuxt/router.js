import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _1d44ff18 = () => interopDefault(import('../pages/b2.vue' /* webpackChunkName: "pages/b2" */))
const _867859ec = () => interopDefault(import('../pages/b2table.vue' /* webpackChunkName: "pages/b2table" */))
const _5ca65b3d = () => interopDefault(import('../pages/export-pdf.vue' /* webpackChunkName: "pages/export-pdf" */))
const _38af4ff8 = () => interopDefault(import('../pages/index2.vue' /* webpackChunkName: "pages/index2" */))
const _65f8a9da = () => interopDefault(import('../pages/print-b1.vue' /* webpackChunkName: "pages/print-b1" */))
const _622df51e = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))
const _607753d6 = () => interopDefault(import('../pages/_slug.vue' /* webpackChunkName: "pages/_slug" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/b2",
    component: _1d44ff18,
    name: "b2"
  }, {
    path: "/b2table",
    component: _867859ec,
    name: "b2table"
  }, {
    path: "/export-pdf",
    component: _5ca65b3d,
    name: "export-pdf"
  }, {
    path: "/index2",
    component: _38af4ff8,
    name: "index2"
  }, {
    path: "/print-b1",
    component: _65f8a9da,
    name: "print-b1"
  }, {
    path: "/",
    component: _622df51e,
    name: "index"
  }, {
    path: "/:slug",
    component: _607753d6,
    name: "slug"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
