export const state = () => ({
    posts: [],
    listcategories: []
})

export const mutations = {
    frontPagePosts (state, posts) {
        state.posts = posts
    },
    listCategories (state, listcategories) {
        state.listcategories = listcategories
    }
}